@ECHO OFF

echo Making `bin` directory...
echo.
IF NOT EXIST .\stsg\bin (mkdir .\stsg\bin) ELSE GOTO Exception

:Whatever
echo Moving `res` (game resources) to the `bin` directory...
echo.
xcopy /E /I /Y .\res .\stsg\bin\res

echo.
echo Copying application config file to the `bin` directory...
echo.
xcopy /-Y .\stsg.cfg .\stsg\bin\stsg.cfg
GOTO End

:Exception
echo !!!EXCEPTION!!!
choice /M ".\stsg\bin already exists and may contain files. Continue anyway?"
echo You may prompted more questions regarding any conflicts...
echo.
IF ERRORLEVEL 2 GOTO End
GOTO Whatever

:End
