#!/bin/bash
#

if [[ ! -d ./cmake || ! -d ./cmake/Modules ]]
then
	mkdir -p ./cmake/Modules
fi

ourpwd=`pwd`
ln -s "${ourpwd}/contrib/sfml/cmake/Modules/FindSFML.cmake" ./cmake/Modules/FindSFML.cmake

