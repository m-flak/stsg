#include "ui_base.h"

UIBase::UIBase()
{
	m_visible = true;
	m_aligntype = UIBase::NonAlignable;
	m_xpadding = 0.0f;
	m_ypadding = 0.0f;
}

UIBase::UIBase(uint32_t align)
{
	m_visible = true;
	m_aligntype = align;
	m_xpadding = 0.0f;
	m_ypadding = 0.0f;
}

UIBase::UIBase(uint32_t align, bool visible)
{
	m_visible = visible;
	m_aligntype = align;
	m_xpadding = 0.0f;
	m_ypadding = 0.0f;
}

UIBase::UIBase(uint32_t align, bool visible, float xpad, float ypad)
{
	m_visible = visible;
	m_aligntype = align;
	m_xpadding = xpad;
	m_ypadding = ypad;
}

UIBase::~UIBase()
{

}

bool UIBase::isVisible() const
{
	return m_visible;
}

uint32_t UIBase::getAlignment() const
{
	return m_aligntype;
}

void UIBase::setVisible(bool visibility)
{
	m_visible = visibility;
}

void UIBase::setAlignment(uint32_t alignment)
{
	m_aligntype = alignment;
}

float UIBase::getPadding(uint8_t padtype) const
{
	if (padtype == UI_XPADDING)
	{
		return m_xpadding;
	}
	else if (padtype == UI_YPADDING)
	{
		return m_ypadding;
	}

	return 0.0f;
}

void UIBase::setPadding(float xpad, float ypad)
{
	m_xpadding = xpad;
	m_ypadding = ypad;
}

void UIBase::getAlignedPosition(sf::Vector2f &fixedpos, float surfaceW, float surfaceH, float myW, float myH, float myX, float myY)
{
	const uint32_t myalignment = getAlignment();
	float halfw, halfh;

	// myX and myY are the origin of this op
	fixedpos.x = myX;
	fixedpos.y = myY;

	if (myalignment == 0)
		return;

	// X ADJUSTMENTS
	if (myalignment & UIBase::AlignLeft)
	{
		fixedpos.x += 0;
	}
	else if (myalignment & UIBase::AlignRight)
	{
		fixedpos.x += surfaceW-myW;
	}

	// FIXME:: I am def. sure this is bad math.
	if (myalignment & UIBase::AlignCenter)
	{
		halfw = myW/2;
		fixedpos.x += fixedpos.x / 2;
		fixedpos.x += 0.5f*surfaceW-halfw;
	}

	// Y ADJUSTMENTS
	if (myalignment & UIBase::AlignTop)
	{
		fixedpos.y += 0;
	}
	else if (myalignment & UIBase::AlignBottom)
	{
		fixedpos.y += surfaceH-myH;
	}

	// FIXME:: I am def. sure this is bad math.
	if (myalignment & UIBase::AlignCenterVert)
	{
		halfh = myH/2;
		fixedpos.y += 0.5f*surfaceH-halfh;
	}

	// DO PADDING IF ANY
	if (m_xpadding != 0.0f || m_ypadding != 0.0f)
	{
		fixedpos.x += m_xpadding;
		fixedpos.y += m_ypadding;
	}

}
