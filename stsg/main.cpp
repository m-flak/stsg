#include <SFML/Graphics.hpp>
#include "configuration.h"
#include "fontmgr.h"
#include "scenemgr.h"
#include "mainmenu.h"
#include "misc.h"

/*
 * * WINDOWS SPECIFIC HARDWARE-CURSOR OVERRIDE
 */
#ifdef WIN32
WNDPROC g_originalproc; // WndProc for the SFML-created window
HCURSOR g_newcursor;    // The game's cursor

/* Our "sub-classed" WndProc of the SFML-created window.
 * * Changes the cursor & returns control to g_originalproc for rest of win32 bs
 */
LRESULT APIENTRY CursorSubProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
#endif

int main(int argc, char **argv)
{
	/* * * BEGIN CONFIGURATION * * */
	unsigned int winflags = sf::Style::Titlebar | sf::Style::Close; //default unless FS
	ConfigData myconfig = {0};

	// setup environment paths
	setupPaths();

	if (!loadConfiguration("stsg.cfg", &myconfig))
	{
		return 1;
	}
	if (myconfig.fullscreen)
	{
		winflags = sf::Style::Fullscreen;
	}
	/* * * END CONFIGURATION * * */

	sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
	sf::RenderWindow window(sf::VideoMode(myconfig.screen_width, myconfig.screen_height, desktop.bitsPerPixel), "STSG", winflags);
	window.setVerticalSyncEnabled(true);

	/***CURSOR: WINDOWS
	  * "subclasses" the sfml window via win32api to override the cursor
	****/
	#ifdef WIN32
		setupWinCursor(window.getSystemHandle(), (LPDWORD) &g_originalproc, &g_newcursor, CursorSubProc);
	#endif

	// Load fonts
	FontManager::initialize();
	// Start SceneManager
	SceneManager::initialize();

	// Give SceneManager a copy of our parsed configuration
	SceneManager::setConfig(myconfig);

	/* MOUSE POSITION INFO-TEXT SETUP */
	bool display_mousepos = false;
	sf::Text mousepostxt;
	mousepostxt.setFont(FontManager::getInstance()->getFontRef(FontManager::UIReg));
	mousepostxt.setColor(sf::Color::Red);
	
	/* FOR EVENT LOOP & TIMING */
	sf::Clock clock;
	sf::Event event;

	SceneManager::getInstance()->addScene(new MainMenu, &window, &event, true);
	while (window.isOpen())
	{
		sf::Event event2(event);
		event = event2;

		while (window.pollEvent(event))
		{
			/* INPUT:: CLICKED THE WINDOW'S CLOSE BUTTON */
			/* INPUT:: KEY[F12] SAFE EXIT KEY */
			if ((event.type == sf::Event::Closed) ||
				((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::F12)))
			{
				window.close();
				break;
			}
			
			/* INPUT:: KEY[F10] TOGGLE MOUSE POS DISPLAY */
			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::F10)
			{
				display_mousepos = (!display_mousepos) ? true : false;
			}

			SceneManager::getInstance()->updateScene();
		}

		window.clear();

		SceneManager::getInstance()->drawScene();

		// simply returns/does nothing if not toggled
		updateMouseInfoText(&display_mousepos, &window, &mousepostxt);

		window.display();
	}

	// SCENE CLEANUP
	SceneManager::getInstance()->killAllScenes();

	// CLEANUP ENVIRONMENT PATHS
	delete[] g_datapath;
	delete[] g_savepath;

	return 0;
}

#ifdef WIN32
LRESULT APIENTRY CursorSubProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_SETCURSOR)
	{
		SetCursor(g_newcursor);
		return TRUE;
	}

	return CallWindowProc(g_originalproc, hWnd, uMsg, wParam, lParam);
}
#endif
