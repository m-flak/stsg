#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

#include <cstdint>

/* !!!HEY YOU!!!
 * * * * * * * * *
 * Be sure to change this when adding more values to parse.
 * * * * * * * * * 
 */
#define CFG_TOTAL_OPTIONS 7

#ifdef WIN32
#define PATH_SEPARATOR "\\"
#define HOME_DIRECTORY "USERPROFILE"
#else
#define PATH_SEPARATOR "/"
#define HOME_DIRECTORY "HOME"
#endif

static char *g_datapath = NULL;
static char *g_savepath = NULL;

struct ConfigData
{
	int  screen_width;
	int  screen_height;
	bool fullscreen;
	uint32_t color_uifill;
	uint32_t color_uiborder;
	uint32_t color_uitext;
	uint32_t color_uitexthover;
};

bool loadConfiguration(const char *cfgfile, ConfigData *cfgdata /*OUT*/);

// initializes g_datapath & g_savepath
// / they must be freed later
void setupPaths();

#endif
