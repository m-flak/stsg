#ifndef _SAVESYS_H_
#define _SAVESYS_H_

#include <string>
#include <vector>
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

class SaveFile
{
public:
	SaveFile();
	SaveFile(const std::string &savpath);
	SaveFile(const std::string &savpath, const std::string &savname);
	virtual ~SaveFile();

	std::string savepath;
	std::string savename;

	/*
	 * SaveFile will just store the filename and such for later operations.
	 * The methods for these operations will be contained here.
	 * * *
	 * These methods will read the save @ `savepath` and create-n-fill a class containing
	 *  a loaded game's variables/globals/etc.
	 */
};

class SaveSystem
{
public:
	SaveSystem();
	virtual ~SaveSystem();

	const int getNumSaves() const;

	void populate();

	SaveFile &getNthSave(const int index);

private:
	bfs::path	p_savpath;
	std::vector<SaveFile> p_saves;
	int			p_numsaves;
};


#endif
