#include <sstream>
#include <SFML/Window/Mouse.hpp>
#include "misc.h"

void updateMouseInfoText(bool *doupdate, sf::RenderWindow *window, sf::Text *muhtext)
{
	if (!(*doupdate))
		return;

	std::ostringstream oss;
	sf::Vector2i vmpos;

	vmpos = sf::Mouse::getPosition(*window);
	oss << "Mouse Position: " << vmpos.x << ',' << vmpos.y;
	muhtext->setString(oss.str());

	window->draw(*muhtext);
}

#ifdef WIN32

void APIENTRY setupWinCursor(HWND window, LPDWORD oldwinproc, HCURSOR *thecursor, WNDPROC newproc)
{
	HCURSOR cursor = LoadCursor(GetModuleHandle(NULL), MAKEINTRESOURCE(101)); // 101 - IDC_CURSOR1 in rsrc.rc

	// store old wndproc
	*oldwinproc = GetWindowLongPtr(window, GWLP_WNDPROC);
	// store the cursor
	*thecursor  = cursor;

	SetWindowLongPtr(window, GWLP_WNDPROC, (LONG) newproc);

}

#endif
