#ifndef _SCENEMGR_H_
#define _SCENEMGR_H_

#include <stack>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "scene_base.h"
#include "configuration.h"

class SceneManager
{
public:
	// gets/creates scenemanager
	static SceneManager *getInstance();

	// copies (and creates dest.) a populated configdata structure
	static void setConfig(const ConfigData &cfgdat);

	// returns ptr to configdata structure
	// THROWS EXCEPTION IF SETCONFIG HASN'T BEEN CALLED
	static ConfigData *getConfig();

	// for clarity & convenience
	static inline void initialize() { getInstance(); }

	// adds a scene to the top of stack
	// for transitions, add next first, then the to-be current
	// you allocate, manager kills in call to killScene
	void addScene(Scene *addedscene, sf::RenderWindow *renderparent, sf::Event *eventhandler, bool loadme);

	// kills/deallocates top scene on stack
	// calls unload in scene
	void killScene();

	// kills all scenes
	// you shouldnt have set loadme for all scenes in stack
	// if you did, you are retarded. only top is checked for being loaded & gets its unload called
	void killAllScenes();

	// draws current scene
	void drawScene();

	// updates current scene
	void updateScene();

private:
	static SceneManager *p_instance;
	static ConfigData   *p_configuration;

	std::stack<Scene*>   p_scenes;
	bool                 p_sceneloaded;

	// // // // //
	// CONSTRUCTOR
	SceneManager();

};

#endif
