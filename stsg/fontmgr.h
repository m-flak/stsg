#ifndef _FONTMGR_H_
#define _FONTMGR_H_

#define FM_MAXFONTS 3

#include <array>
#include <SFML/Graphics/Font.hpp>

class FontManager
{
public:
	enum TypeFont
	{
		Title  = 0,
		UIReg  = 1,
		UIHead = 2
	};

	// gets/creates instance of fontmanager
	// fonts are loaded on creation of manager
	static FontManager *getInstance();

	// for code clarity
	static inline void initialize() { getInstance(); }

	sf::Font& getFontRef(const int type);

private:
	static FontManager                *p_instance;
	std::array<sf::Font, FM_MAXFONTS>  p_fonts;
	const int				           p_numfonts;

	// // // // //
	// CONSTRUCTOR
	FontManager();

	// // // // //
	// LOADED FROM HERE
	void loadAllFonts();
};

#endif
