#ifdef WIN32
    #include <windows.h>
#endif
#include <GL/gl.h>
#include "tiled_bg.h"

TiledBG::TiledBG()
{
	p_loaded = false;
	p_bgcoords = new float[6];
}

TiledBG::~TiledBG()
{
	delete[] p_bgcoords;
}

TiledBG *TiledBG::createAndLoad(const sf::String &respath, const sf::Vector2u &bgsize)
{
	TiledBG *constructed = new TiledBG;
	sf::Vector2u texsize;

	constructed->p_loaded = constructed->p_bgtex.loadFromFile(respath);
	// SFML will handle OpenGL texture setup; make it repeated
	constructed->p_bgtex.setRepeated(true);
	texsize = constructed->p_bgtex.getSize();
	constructed->p_bgcoords[0] = (float) bgsize.x;						// 0 - SCREENWIDTH
	constructed->p_bgcoords[1] = (float) bgsize.y;						// 1 - SCREENHEIGHT
	constructed->p_bgcoords[2] = (float) texsize.x;						// 2 - TEXTUREWIDTH
	constructed->p_bgcoords[3] = (float) texsize.y;						// 3 - TEXTUREHEIGHT
	constructed->p_bgcoords[4] = (float) bgsize.x / (float) texsize.x;  // 4 - TEXTURERIGHT
	constructed->p_bgcoords[5] = (float) bgsize.y / (float) texsize.y;  // 5 - TEXTUREBOTTOM

	return constructed;
}

bool TiledBG::hasLoaded() const
{
	return p_loaded;
}

void TiledBG::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (!hasLoaded())
		return;

	sf::Texture::bind(&p_bgtex);
	// Reset transform
	glLoadIdentity();
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(0.0f, 0.0f);
		glTexCoord2f(p_bgcoords[4] /*TextureRight*/, 0.0f);
		glVertex2f(p_bgcoords[0] /*ScreenWidth*/, 0.0f);
		glTexCoord2f(p_bgcoords[4] /*TextureRight*/, p_bgcoords[5] /*TextureBottom*/);
		glVertex2f(p_bgcoords[0] /*ScreenWidth*/, p_bgcoords[1] /*ScreenHeight*/);
		glTexCoord2f(0.0f, p_bgcoords[5] /*TextureBottom*/);
		glVertex2f(0.0f, p_bgcoords[1] /*ScreenHeight*/);
	glEnd();
	sf::Texture::bind(NULL);
}

