#ifndef _ACTIVE_SELECTOR_HPP
#define _ACTIVE_SELECTOR_HPP

/* ActiveSelector -- utility for which an Item is 'selectively active'
 *
 * Stores a pointer. The 'Active' element is determined thru pointer comparison
 */
template <class T>
class ActiveSelector
{
public:
	typedef T* selected_ptr;

	ActiveSelector()
	{
		active_element = nullptr;
	}

	ActiveSelector(selected_ptr active)
	{
		active_element = active;
	}

	void setActive(selected_ptr make_active)
	{
		active_element = make_active;
	}

	selected_ptr getActive()
	{
		return active_element;
	}

	bool isActive(selected_ptr to_check)
	{
		return (active_element == to_check);
	}
	
	void clear()
	{
		active_element = nullptr;
	}
	
	bool isCleared()
	{
		return (active_element == nullptr);
	}
private:
	selected_ptr	active_element;
};

#endif
