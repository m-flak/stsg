#include "fontmgr.h"

FontManager *FontManager::p_instance = 0;

FontManager *FontManager::getInstance()
{
	if (!p_instance)
	{
		p_instance = new FontManager;
		p_instance->loadAllFonts();
	}

	return p_instance;
}

sf::Font &FontManager::getFontRef(const int type)
{
	return p_fonts.at(type);
}

FontManager::FontManager()
	: p_numfonts(FM_MAXFONTS)
{
	//bleh
}

void FontManager::loadAllFonts()
{
	p_fonts[0].loadFromFile("res/skirmisherlaser.ttf");
	p_fonts[1].loadFromFile("res/segoeui.ttf");
	p_fonts[2].loadFromFile("res/segoeuib.ttf");

}
