#ifndef _MAINMENU_H_
#define _MAINMENU_H_

#include <SFML/System/String.hpp>
#include "scenemgr.h"
#include "scene_base.h"
#include "ui_dialog.h"
#include "ui_menu.h"
#include "tiled_bg.h"
#include "active_selector.hpp"
#include "savesys.h"
#include "ui_controls.h"

class NewGameDialog : public UIDialog
{
public:
	NewGameDialog();
	virtual ~NewGameDialog();

	UIControls::Text		prompt1;
	UIControls::Text		player_label;
	UIControls::Text		group_label;
	UIControls::TextInput   player_name;
	UIControls::TextInput   group_name;
	
	// set to nullptr unless a text input is active
	ActiveSelector<UIControls::Control> active_input;
	
	// EVENTS FOR CHILDREN CONTROLS
	void hoverTextBox(void *control, void *param);
	void unhoverTextBox(void *control, void *param);
	void clickedTextBox(void *control, void *param);

	void clearEnteredStuff();
};

class MainMenu : public Scene
{
public:
	MainMenu();
	virtual ~MainMenu();

	//IMPLEMENTS CLASS: SCENE
	virtual void update();
	virtual void draw();
	virtual void load();
	virtual void unload();

	//IMPLEMENTS CLASS: SCENE
	virtual sf::RenderWindow *getWindow() const;
	virtual void setWindow(sf::RenderWindow *window);
    virtual sf::Event *getEventHand() const;
	virtual void setEventHand(sf::Event *event);

	// RMB CLICK EVENT
	void rightClickTransition(UIBase *to, UIBase *from);

	// MAIN MENU EVENTS
	void clickedNewGame(void *param);
	void clickedLoadGame(void *param);
	void clickedQuitGame(void *param);

	// LOAD MENU EVENTS
	void clickedLoadSave(void *param);
	void clickedReturnFromLoad(void *param);

private:
	sf::String p_maintitle;
	sf::String p_newtitle;
	sf::String p_loadtitle;
	sf::String p_quittitle;

	// COLORS
	sf::Color  p_fillui;
	sf::Color  p_borderui;
	sf::Color  p_textui;
	sf::Color  p_texthoverui;

	// FOR DETERMINATING THE ACTIVE UI FOR DRAWING & EVENTS
	ActiveSelector<UIBase> p_activemenu;

	// DRAWABLES
	TiledBG		    *p_background;
	sf::Text         p_titletext;
	UIMenu	         p_menuwidget;
	UIMenu		    *p_loadmenuwidget;
	NewGameDialog	*p_newgamedialog;

	// SAVE SYSTEM
	SaveSystem  p_savesys;

	//IMPLEMENTS CLASS: SCENE
	sf::RenderWindow *p_window;
	sf::Event		 *p_eventhandler;

	/* Constructs & Creates the load game menu
	 * * The load game menu is created non-visible
	 */
	UIMenu *createLoadMenu(float sizex, float sizey, float padx, float pady);
	
	NewGameDialog *createNewGameDlg(const sf::String &dlgtitle, float sizex, float sizey);
};

#endif
