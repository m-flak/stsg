#ifndef _UI_BASE_H_
#define _UI_BASE_H_

#include <SFML/System/Vector2.hpp>
#include <cstdint>

#define UI_XPADDING 0
#define UI_YPADDING 1

class UIBase
{
public:
	enum AlignType
	{
		NonAlignable = 0,
		AlignLeft = 1,
		AlignRight = 2,
		AlignCenter = 4,
		AlignTop = 8,
		AlignBottom = 16,
		AlignCenterVert = 32
	};

	UIBase();
	explicit UIBase(uint32_t align);
	UIBase(uint32_t align, bool visible);
	UIBase(uint32_t align, bool visible, float xpad, float ypad);
	
	virtual ~UIBase();

	bool isVisible() const;
	uint32_t getAlignment() const;

	virtual void setVisible(bool visibility);
	void setAlignment(uint32_t alignment);

	float getPadding(uint8_t padtype) const;
	void  setPadding(float xpad, float ypad);

	//forgive me father for this function
	void getAlignedPosition(sf::Vector2f &fixedpos, float surfaceW, float surfaceH, float myW, float myH, float myX, float myY);

protected:
	bool m_visible;
	uint32_t m_aligntype;
	float m_xpadding;
	float m_ypadding;
};

#endif
