#ifndef _MISC_H_
#define _MISC_H_

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#ifdef WIN32
#include <Windows.h>

void APIENTRY setupWinCursor(HWND window, LPDWORD oldwinproc, HCURSOR *thecursor, WNDPROC newproc);

#endif

void updateMouseInfoText(bool *doupdate, sf::RenderWindow *window, sf::Text *muhtext);

#endif
