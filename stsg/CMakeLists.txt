set(STSG_SOURCES
	configuration.cpp
	fontmgr.cpp
	main.cpp
	mainmenu.cpp
	misc.cpp
	savesys.cpp
	scenemgr.cpp
	tiled_bg.cpp
	ui_base.cpp
	ui_controls.cpp
	ui_dialog.cpp
	ui_menu.cpp
)
set(STSG_HEADERS
	active_selector.hpp
	configuration.h
	fontmgr.h
	mainmenu.h
	misc.h
	savesys.h
	scene_base.h
	scenemgr.h
	tiled_bg.h
	ui_base.h
	ui_controls.h
	ui_dialog.h
	ui_menu.h
)

add_executable(stsg ${STSG_HEADERS} ${STSG_SOURCES}) 
target_link_libraries(stsg ${STSG_LINK_LIBS})
set_property(TARGET stsg PROPERTY CXX_STANDARD 11)
set_target_properties(stsg PROPERTIES COMPILE_FLAGS -fpermissive)

