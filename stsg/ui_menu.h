#ifndef _UI_MENU_H_
#define _UI_MENU_H_

#include <cstdint>
#include <vector>
#include <functional>
#include <SFML/Graphics.hpp>
#include <SFML/System/String.hpp>
#include "fontmgr.h"
#include "ui_base.h"

#define MENTRY_DIMENSIONX 0
#define MENTRY_DIMENSIONY 1

class UIMenuEntry : public sf::Transformable, public UIBase
{
	typedef FontManager::TypeFont EntryStyle;
public:
	// see `UIMenu::addMenuEntry` for details about these args
	UIMenuEntry(const sf::String &refid, const sf::String &entrystring, EntryStyle textstyle, AlignType textalign, int textsize);
	virtual  ~UIMenuEntry();

	sf::Text   caption;
	sf::String caption_string;
	sf::String referenceid;
	sf::Color  highlighted;
	sf::Color  unhighlighted;

	// Returns the X or Y axis coords of the entry's dimensions
	float getEntryDimension(uint8_t xory);

	//OVERRIDES
	const sf::Vector2f &getPosition() const;
	const sf::Vector2f &getOrigin() const;
	const sf::Vector2f &getScale() const;

	//EVENTS
	std::function<void(void*)> event_clicked;

};

class UIMenu : public sf::Transformable, public sf::Drawable, public UIBase
{
	typedef FontManager::TypeFont EntryStyle;
public:
	UIMenu();
	virtual ~UIMenu();

	// Set the background color of the UIMenu. The default, if unset, is opaque white
	void setMenuColor(const sf::Color &bgcolor);

	// Set the color & thickness of a border for the UIMenu.
	// By default, there is no border.
	void setMenuBorder(const sf::Color &bordercolor, float pxthick);

	// Set Dimensions of the UIMenu. The base Rectangle surface will be drawn according to this.
	// Children of the UIMenu will be constrained to these dimensions
	void setDimensions(float posX, float posY, float sizeW, float sizeH);
	
	// Padding appplied to each entry for x & y
	// FIXME:: changing entry padding after getalignedposition is called in entry internals does nothing
	void setMenuEntryPad(const sf::Vector2f &padding);

	// refkey - Reference ID, look up by string, in future add serialization support for ui classes w/ these as id's
	// entrycaption - Caption
	// font - see `fontmgr.h` for values
	// size - character size in pixels, passed to the sf::Text ctor
	// norm - normal color
	// hilite  - hover color for mouseover
	void addMenuEntry(const sf::String &refkey, const sf::String &entrycaption, EntryStyle font, AlignType alignment, int size, const sf::Color &norm, const sf::Color &hilite);

	// Attempt to get the entry under the mouse
	// Returns -1 if no suitable entry found
	int getEntryByMouse(const sf::Vector2f &mousePos);

	// Returns number of menu entries
	const int getNumEntries() const;

	// Toggle highlight color on MenuEntry at index `entry`
	void highlightEntry(const int entry);

	// Call the function bound to the event_clicked member of the MenuEntry at index `entry`
	void handleEntryClicked(const int entry, void *param);

	//Overrides
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	//Operators
	// THROWS: std::out_of_range -- Make sure to enclose in exception handler
	UIMenuEntry *operator[](sf::String id) const;
	// THROWS: std::out_of_range -- Make sure to enclose in exception handler
	UIMenuEntry *operator[](const int index) const;
private:
	sf::Rect<float>    p_dimensions;			/* Our own store of dimens. p_mainsurface is built accord. to these & entries are aligned to these */
	sf::RectangleShape p_mainsurface;			/* Menu background/What the user sees as the GUI object for menu. */
	float              p_offsety;				/* THE UIMENU IS ONLY VERTICAL FOR NOW. *** This is the offset for each entry's coords */
	sf::Vector2f       p_entriespadding;		/* Padding to apply to X & Y axii for each entry. padding is great hack for broken align func, atm */ 
	std::vector<UIMenuEntry> p_entries;			/* The menu entries associated with this menu instance */
	int				   p_largestdmx_x;			/* The largest encountered X axii character size for an added menu entry */
};

#endif
