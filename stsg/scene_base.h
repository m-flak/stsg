#ifndef _SCENE_BASE_H
#define _SCENE_BASE_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

class Scene
{
public:
	virtual void update() = 0;
	virtual void   draw() = 0;
	virtual void   load() = 0;
	virtual void unload() = 0;

	// Data
	virtual sf::RenderWindow *getWindow() const = 0;
	virtual void setWindow(sf::RenderWindow *window) = 0;
	virtual sf::Event *getEventHand() const = 0;
	virtual void setEventHand(sf::Event *event) = 0;
};

#endif
