#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "configuration.h"

#ifdef WIN32
#pragma warning(push)
#pragma warning(disable: 4996)
#include <ShlObj.h>
#endif

bool loadConfiguration(const char *cfgfile, ConfigData *cfgdata)
{
	int  totalopts = CFG_TOTAL_OPTIONS;		// TOTAL OPTIONS - CHANGE WHEN ADDING!
	int  foundopts = 0;
	char *parambuf = NULL;
	char *valuebuf = NULL;
	FILE *config = fopen(cfgfile, "r");

	if (!config)
		return false;

	parambuf = new char[128];
	valuebuf = new char[128];

	while (feof(config) == 0 && foundopts < totalopts)
	{
		memset(parambuf, '\0', 128);
		memset(valuebuf, '\0', 128);

		fscanf(config, "%s %s", parambuf, valuebuf);

		if (strstr(parambuf, "width"))
		{
			cfgdata->screen_width = atoi(valuebuf);
			foundopts++;
		}
		else if (strstr(parambuf, "height"))
		{
			cfgdata->screen_height = atoi(valuebuf);
			foundopts++;
		}
		else if (strstr(parambuf, "fullscreen"))
		{
			int i = atoi(valuebuf);
			cfgdata->fullscreen = i&1;
			foundopts++;
		}
		else if (strstr(parambuf, "ui-fill-rgba"))
		{
			uint32_t u = 0;
			sscanf(valuebuf, "%x", &u);
			cfgdata->color_uifill = u;
			foundopts++;
		}
		else if (strstr(parambuf, "ui-border-rgba"))
		{
			uint32_t u = 0;
			sscanf(valuebuf, "%x", &u);
			cfgdata->color_uiborder = u;
			foundopts++;
		}
		else if (strstr(parambuf, "ui-text-rgba"))
		{
			uint32_t u = 0;
			sscanf(valuebuf, "%x", &u);
			cfgdata->color_uitext = u;
			foundopts++;
		}
		else if (strstr(parambuf, "ui-texthover-rgba"))
		{
			uint32_t u = 0;
			sscanf(valuebuf, "%x", &u);
			cfgdata->color_uitexthover = u;
			foundopts++;
		}
	}


	delete[] parambuf;
	delete[] valuebuf;
	return true;
}

void setupPaths()
{
	const char *appdir = ".stsg";
	const char *savdir = "saves";
	size_t     length  = 0;
	
	length = strlen(getenv(HOME_DIRECTORY)) + 2; //plus the slash & null
	length += strlen(appdir);
	
	g_datapath = new char[length];
	strcpy(g_datapath, getenv(HOME_DIRECTORY));
	strcat(g_datapath, PATH_SEPARATOR);
	strcat(g_datapath, appdir);
	
	length = strlen(g_datapath) + 2;
	length += strlen(savdir);
	
	g_savepath = new char[length];
	strcpy(g_savepath, g_datapath);
	strcat(g_savepath, PATH_SEPARATOR);
	strcat(g_savepath, savdir);
	
	// CREATE THE DIRECTORIES ON WINDOWS
	#ifdef WIN32
	char *dirs[2] = {g_datapath, g_savepath};
	int ds = 0;
	int ret = ERROR_SUCCESS;
	while (ds < 2 && ret == ERROR_SUCCESS)
	{
		ret = SHCreateDirectoryEx(NULL, dirs[ds], NULL);
		ds++;
	}
	#endif
}

#ifdef WIN32
#pragma warning(pop)
#endif
