#include "ui_dialog.h"

/* default construction shall:
 * * render an invisible dialog box
 * * give it a default of no alignment
 * * give it a title of "..." @ 14pt
 * * use the font set as `UIHead` in the font manager
 */
UIDialog::UIDialog()
	: UIBase(UIBase::NonAlignable, false)
{
	dialog_title   = "...";
	size_title     = 14;
	
	m_title = new sf::Text(dialog_title, FontManager::getInstance()->getFontRef(FontManager::UIHead), size_title);
	
	m_dialogsize = sf::Vector2f(0.0f,0.0f);
}

UIDialog::~UIDialog()
{
	delete m_title;
}

void UIDialog::setOrigin(const sf::Vector2f &origin)
{
	return sf::Transformable::setOrigin(origin);
}

void UIDialog::setPosition(const sf::Vector2f &position)
{
	m_dialog.setPosition(position);
	m_titlebar.setPosition(position);
	m_title->setPosition(position.x, 2.0f+position.y);
	
	return sf::Transformable::setPosition(position);
}

void UIDialog::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (!isVisible())
		return;
	
	// UPDATE TITLE BAR IF TITLE TEXT CHANGED
	if (dialog_title != m_title->getString() || size_title != m_title->getCharacterSize())
	{
		m_title->setString(dialog_title);
		m_title->setCharacterSize(size_title);
	}
	
	target.draw(m_dialog, states);
	target.draw(m_titlebar, states);
	target.draw(*m_title, states);

	for (auto control = m_controls.begin(); control != m_controls.end(); ++control)
	{
		if (!control->as_ui->isVisible())
			continue;

		target.draw(*control->as_drawn);
	}
	
}

void UIDialog::setVisible(bool visibility)
{
	for (auto control = m_controls.begin(); control != m_controls.end(); ++control)
	{
		control->as_ui->setVisible(visibility);
	}

	this->m_visible = visibility;
}

void UIDialog::setColors(const sf::Color &titletext, const sf::Color &titlebar, const sf::Color &dialog)
{
	m_title->setColor(titletext);
	m_titlebar.setFillColor(titlebar);
	m_dialog.setFillColor(dialog);
}

void UIDialog::setSize(float szX, float szY)
{
	sf::Vector2f tbsize(szX, 4.0f+size_title);
	
	m_dialogsize.x = szX;
	m_dialogsize.y = szY;
	
	m_dialog.setSize(m_dialogsize);
	m_titlebar.setSize(tbsize);
}
	
bool UIDialog::getControlFromMouse(const sf::Vector2f &mouse, UIControls::Control **control)
{
	for (auto controls = m_controls.begin(); controls != m_controls.end(); ++controls)
	{
		// We only care about visible controls at the mouse pointer
		if (controls->as_ui->isVisible() && controls->as_ui->insideControl(mouse))
		{
			*control = controls->as_ui;
			return true;
		}
	}
	
	return false;
}
