#include <cstdint>
#include <cstring>
#include "fontmgr.h"
#include "ui_controls.h"

UIControls::Control::Control()
{
	const sf::Vector2f vec(0.0f,0.0f);
	
	m_position = vec;
	m_sizeof   = vec;
	
	memset(UIControls::Control::mapped_events, false, sizeof(bool)*UIC_EVENTS);
}

UIControls::Control::~Control()
{

}

void UIControls::Control::setPosition(const sf::Vector2f &pos)
{
	m_position = pos;
	m_rect.left = pos.x;
	m_rect.top  = pos.y;
	// for class children overrides:
	// call me
	// do your stuff next
}

void UIControls::Control::setPosition(float x, float y)
{
	m_position = sf::Vector2f(x, y);
	m_rect.left = x;
	m_rect.top  = y;
}

const sf::Vector2f &UIControls::Control::getSize() const
{
	return m_sizeof;
}

bool UIControls::Control::insideControl(const sf::Vector2f &point) const
{
	return m_rect.contains(point);
}

/* *********************************************************************
 * BEGIN UIControls::Text
 * *********************************************************************
 */

UIControls::Text::Text()
{
	p_text = new sf::Text();
	p_contents = new sf::String();
}

UIControls::Text::Text(const UIControls::Text &other)
{
	/* UIBase's content */
	this->m_aligntype = other.m_aligntype;
	this->m_visible = other.m_visible;
	this->m_xpadding = other.m_xpadding;
	this->m_ypadding = other.m_ypadding;

	/* Control's content */
	this->m_position = other.m_position;
	this->m_sizeof = other.m_sizeof;
	this->m_rect   = other.m_rect;
	
	/* Our Content */
	this->p_text = new sf::Text(*other.p_text);
	this->p_contents = new sf::String(*other.p_contents);

	this->p_text->setString(*this->p_contents);

	this->p_sizetext = other.p_sizetext;
}

UIControls::Text::~Text()
{
	delete p_text;
	delete p_contents;
}

void UIControls::Text::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (!isVisible())
		return;

	if (p_text->getString() != *p_contents)
		p_text->setString(*p_contents);

	target.draw(*p_text, states);

}

void UIControls::Text::setPosition(const sf::Vector2f &pos)
{
	UIControls::Control::setPosition(pos);
	p_text->setPosition(m_position);
}

void UIControls::Text::setPosition(float x, float y)
{
	UIControls::Control::setPosition(x, y);
	p_text->setPosition(m_position);
}

void UIControls::Text::setContents(const sf::String &str)
{
	p_text->setString(str);
	*p_contents = str;
}

void UIControls::Text::setContents(const sf::String &str, int ptSize)
{
	p_sizetext = ptSize;
	p_text->setCharacterSize(p_sizetext);
	return setContents(str);
}

void UIControls::Text::setStyle(const sf::Color &color, const sf::Font &font)
{
	p_text->setColor(color);
	p_text->setFont(font);
}

void UIControls::Text::setStyle(const sf::Color &color, const sf::Font &font, int ptSize)
{
	p_sizetext = ptSize;
	p_text->setCharacterSize(p_sizetext);
	return setStyle(color, font);
}

const sf::Vector2f &UIControls::Text::getSize() const
{
	float x = (float) p_contents->getSize()*p_sizetext;
	float y = (float) p_sizetext;

	// the above calculates the size incorrectly
	// / divide by 2 to get correct size
	m_sizeof = sf::Vector2f(x/2,y);
	m_rect.width = x/2;
	m_rect.height = y;
	return m_sizeof;
}

/* *********************************************************************
 * BEGIN UIControls::TextInput
 * *********************************************************************
 */

UIControls::TextInput::TextInput(const int max_length)
{
	const uint32_t bgcolor = 0x00000033; // black @ 20% opacity
	const int default_ptsize = 14;
	const float box_x = (float) max_length*default_ptsize;
	const float thick = 1.0f;
	
	p_bgcolor = sf::Color(bgcolor);
	p_fgcolor = sf::Color::White;
	p_bordcolor = sf::Color::Black;
	
	p_thebox = new sf::RectangleShape(sf::Vector2f(thick+box_x, (float) default_ptsize+thick));
	p_thebox->setFillColor(p_bgcolor);
	p_thebox->setOutlineColor(p_bordcolor);
	p_thebox->setOutlineThickness(thick);

	p_text = new sf::Text;
	p_text->setCharacterSize(default_ptsize);
	p_text->setColor(p_fgcolor);
	p_text->setFont(FontManager::getInstance()->getFontRef(FontManager::UIReg));

	m_sizeof = p_thebox->getSize();
	m_rect.width = m_sizeof.x;
	m_rect.height = m_sizeof.y;
}

UIControls::TextInput::~TextInput()
{
	delete p_thebox;
	delete p_text;
}

void UIControls::TextInput::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (!isVisible())
		return;
	
	p_text->setString(buffer);

	target.draw(*p_thebox, states);
	target.draw(*p_text, states);
}

void UIControls::TextInput::setPosition(const sf::Vector2f &pos)
{
	UIControls::Control::setPosition(pos);
	p_thebox->setPosition(m_position);
	p_text->setPosition(m_position);
}

void UIControls::TextInput::setPosition(float x, float y)
{
	UIControls::Control::setPosition(x, y);
	p_thebox->setPosition(m_position);
	p_text->setPosition(m_position);
}

void UIControls::TextInput::setColors(const sf::Color &bg, const sf::Color &fg, const sf::Color &brd)
{
	p_bgcolor = bg;
	p_fgcolor = fg;
	p_bordcolor = brd;
	p_thebox->setFillColor(p_bgcolor);
	p_text->setColor(p_fgcolor);
	p_thebox->setOutlineColor(p_bordcolor);
}

void UIControls::TextInput::setColors(const sf::Color &fg, const sf::Color &brd)
{
	return setColors(p_bgcolor, fg, brd);
}

void UIControls::TextInput::setColors(const int setcolor, const sf::Color &set_to)
{
	switch (setcolor)
	{
		case UIControls::TextInput::BGColor:
			return setColors(set_to, p_fgcolor, p_bordcolor);
		break;
		case UIControls::TextInput::FGColor:
			return setColors(p_bgcolor, set_to, p_bordcolor);
		break;
		case UIControls::TextInput::BDColor:
			return setColors(p_bgcolor, p_fgcolor, set_to);
		break;
		default:
		break;
	}
	
	return;
}

const sf::Vector2f &UIControls::TextInput::getSize() const
{
	const sf::Vector2f vec = p_thebox->getSize();

	if (m_sizeof != vec)
	{
		m_sizeof = vec;
		m_rect.width = vec.x;
		m_rect.height  = vec.y;
	}
	
	return m_sizeof;
}

/* *********************************************************************
 * BEGIN UIControls::Button
 * *********************************************************************
 */

UIControls::Button::Button()
{

}

UIControls::Button::~Button()
{

}

void UIControls::Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{

}

void UIControls::Button::setPosition(const sf::Vector2f &pos)
{

}

void UIControls::Button::setPosition(float x, float y)
{

}

const sf::Vector2f &UIControls::Button::getSize() const
{

}

/* *********************************************************************
 * BEGIN HELPER FUNCTIONS
 * *********************************************************************
 */

void UIControls::input_modify_buffer(UIControls::TextInput *input, uint32_t chr)
{
	if (chr == '\b') // handle backspaces
	{
		int size = 0;
		
		if (input->buffer.isEmpty())
			return;
		
		size = input->buffer.getSize()-1;
		input->buffer.erase(size);
		
		return;
	}

	input->buffer += sf::String(chr);
}
