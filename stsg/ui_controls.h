#ifndef _UI_CONTROLS_H_
#define _UI_CONTROLS_H_

#include <cstdint>
#include <functional>
#include <SFML/Graphics.hpp>
#include "ui_base.h"

// for Control::mapped_events array size
#define UIC_EVENTS 3

namespace UIControls
{
	class Control : public ::UIBase
	{
	public:
		Control();
		virtual ~Control();
		
		virtual void setPosition(const sf::Vector2f &pos);
		virtual void setPosition(float x, float y);
		
		virtual const sf::Vector2f &getSize() const;
	
		bool insideControl(const sf::Vector2f &point) const;
		
		// Parameters for control events should be:
		// // Ptr to the control
		// // Optional data
		std::function<void(void*,void*)>	event_hover;
		std::function<void(void*,void*)>	event_unhover;
		std::function<void(void*,void*)>	event_clicked;
		
		// array of set events
		// 0 - hover
		// 1 - clicked
		// 2 - unhover
		bool mapped_events[UIC_EVENTS];
		
	protected:
		sf::Vector2f m_position;
		mutable sf::Vector2f m_sizeof;
		mutable sf::Rect<float> m_rect;
	};

	class Text : public sf::Drawable, public UIControls::Control
	{
	public:
		Text();
		Text(const Text &other);

		virtual ~Text();

		//Overrides FROM: sf::Drawable
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		//Overrides FROM: UIControls::Control
		virtual void setPosition(const sf::Vector2f &pos);
		virtual void setPosition(float x, float y);
		
		void setContents(const sf::String &str);
		void setContents(const sf::String &str, int ptSize);

		void setStyle(const sf::Color &color, const sf::Font &font);
		void setStyle(const sf::Color &color, const sf::Font &font, int ptSize);

		//Overrides FROM: UIControls::Control
		virtual const sf::Vector2f &getSize() const;
	private:
		sf::Text   *p_text;
		sf::String *p_contents;

		int         p_sizetext;

	};
	
	class TextInput : public sf::Drawable, public UIControls::Control
	{
	public:
		enum SetColor
		{
			BGColor = 0,
			FGColor = 1,
			BDColor = 2
		};
	
		explicit TextInput(const int max_length=32);
		
		virtual ~TextInput();
		
		// MEMBERS
		sf::String buffer;

		//Overrides FROM: sf::Drawable
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		
		//Overrides FROM: UIControls::Control
		virtual void setPosition(const sf::Vector2f &pos);
		virtual void setPosition(float x, float y);

		void setColors(const sf::Color &bg, const sf::Color &fg, const sf::Color &brd);
		void setColors(const sf::Color &fg, const sf::Color &brd);
		void setColors(const int setcolor, const sf::Color &set_to);
		
		//Overrides FROM: UIControls::Control
		virtual const sf::Vector2f &getSize() const;
	private:
		sf::Color p_bgcolor;
		sf::Color p_fgcolor;
		sf::Color p_bordcolor;
		
		int p_maxlength;
		
		sf::RectangleShape *p_thebox;
		sf::Text		   *p_text;

	};
	
	class Button : public sf::Drawable, public UIControls::Control
	{
	public:
		Button();
		virtual ~Button();

		//MEMBERS
		sf::String caption;

		//Overrides FROM: sf::Drawable
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		//Overrides FROM: UIControls::Control
		virtual void setPosition(const sf::Vector2f &pos);
		virtual void setPosition(float x, float y);
		virtual const sf::Vector2f &getSize() const;

	private:

	};

	// HELPER FUNCTIONS
	// //
	// Helper function for modifying the contents of a TextInput's buffer
	void input_modify_buffer(UIControls::TextInput *input, uint32_t chr);
};

#endif
