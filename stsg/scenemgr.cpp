#include <stdexcept>
#include <cstring>
#include "scenemgr.h"

SceneManager *SceneManager::p_instance = 0;
ConfigData   *SceneManager::p_configuration = 0;

SceneManager *SceneManager::getInstance()
{
	if (!p_instance)
	{
		p_instance = new SceneManager;
	}

	return p_instance;
}

void SceneManager::setConfig(const ConfigData &cfgdat)
{
	if (!p_configuration)
	{
		p_configuration = new ConfigData;
		memset(p_configuration, '\0', sizeof(ConfigData));
	}

	*p_configuration = cfgdat;
}

ConfigData *SceneManager::getConfig()
{
	if (!p_configuration)
		throw std::logic_error("NULL CONFIGDATA DEREFERENCE");

	return p_configuration;
}

void SceneManager::addScene(Scene *addedscene, sf::RenderWindow *renderparent, sf::Event *eventhandler, bool loadme)
{
	addedscene->setWindow(renderparent);
	addedscene->setEventHand(eventhandler);

	if (loadme == true)
	{
		addedscene->load();
		p_sceneloaded = true;
	}

	p_scenes.push(addedscene);
}

void SceneManager::killScene()
{
	Scene *killme = p_scenes.top();
	p_scenes.pop();
	killme->unload();
	p_sceneloaded = false;
	delete killme;
}

void SceneManager::killAllScenes()
{
	if (!p_scenes.empty())
	{
		for (unsigned int i=0; i < p_scenes.size(); i++)
		{
			killScene();
		}
	}
}

void SceneManager::drawScene()
{
	Scene *currentscene = p_scenes.top();

	if (p_sceneloaded == false)
	{
		currentscene->load();
	}

	currentscene->draw();
}

void SceneManager::updateScene()
{
	Scene *currentscene = p_scenes.top();

	// do nothing if unloaded
	if (p_sceneloaded == false)
		return;

	currentscene->update();
}

SceneManager::SceneManager()
{
	p_sceneloaded = false;
}
