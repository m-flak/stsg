#ifndef _TILED_BG_H
#define _TILED_BG_H

#include <SFML/Graphics.hpp>
#include <SFML/System/String.hpp>

class TiledBG : public sf::Drawable
{
public:
	virtual ~TiledBG();

	static TiledBG *createAndLoad(const sf::String &respath, const sf::Vector2u &bgsize);

	bool hasLoaded() const;

	// Overrides
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	sf::Texture p_bgtex;
	bool		p_loaded;
	float       *p_bgcoords;

	TiledBG();
};


#endif
