#include <iostream>
#include "mainmenu.h"

#include <SFML/Graphics.hpp>
#include "fontmgr.h"

MainMenu::MainMenu()
{
	const auto cfg = SceneManager::getConfig();

	p_fillui = sf::Color(cfg->color_uifill);
	p_borderui = sf::Color(cfg->color_uiborder);
	p_textui = sf::Color(cfg->color_uitext);
	p_texthoverui = sf::Color(cfg->color_uitexthover);

	p_maintitle = "STSG";
	p_newtitle  = "New  Game";
	p_loadtitle = "Load Game";
	p_quittitle = "Quit Game";
}

MainMenu::~MainMenu()
{

}

void MainMenu::update()
{
	sf::Vector2f mousePos = p_window->mapPixelToCoords(sf::Mouse::getPosition(*p_window));
	UIControls::Control *control = NULL;
	static UIControls::Control *lastcontrol = NULL; // For hover/unhover

	// Handle events for the active menu
	// MAIN MENU
	if (p_activemenu.isActive(&p_menuwidget))
	{
		switch (p_eventhandler->type)
		{
			case sf::Event::MouseMoved:
				p_menuwidget.highlightEntry(p_menuwidget.getEntryByMouse(mousePos));
			break;
			case sf::Event::MouseButtonPressed:
				if (p_eventhandler->mouseButton.button == sf::Mouse::Left)
				{
					mousePos = p_window->mapPixelToCoords(sf::Vector2i(p_eventhandler->mouseButton.x, p_eventhandler->mouseButton.y));
					p_menuwidget.handleEntryClicked(p_menuwidget.getEntryByMouse(mousePos), NULL);
				}
			break;
			default:
			break;
		}
	}

	// LOAD MENU
	if (p_activemenu.isActive(p_loadmenuwidget))
	{
		switch (p_eventhandler->type)
		{
			case sf::Event::MouseMoved:
				p_loadmenuwidget->highlightEntry(p_loadmenuwidget->getEntryByMouse(mousePos));
			break;
			case sf::Event::MouseButtonPressed:
				if (p_eventhandler->mouseButton.button == sf::Mouse::Right)
				{
					rightClickTransition(&p_menuwidget, p_loadmenuwidget);
				}
				if (p_eventhandler->mouseButton.button == sf::Mouse::Left)
				{
					mousePos   = p_window->mapPixelToCoords(sf::Vector2i(p_eventhandler->mouseButton.x, p_eventhandler->mouseButton.y));
					int entnum = p_loadmenuwidget->getEntryByMouse(mousePos);
					p_loadmenuwidget->handleEntryClicked(entnum, (void*)&entnum);
				}
			break;
			default:
			break;
		}
	}

	// NEW GAME DIALOG
	if (p_activemenu.isActive(p_newgamedialog))
	{
		// Have we clicked a textinput box & therefore 'inside' of it?
		// If we are, we shall handle events from here instead of below, skipping
		// // the dialog's event loop entirely with this one.
		// NOTE: isCleared() returns whether or not an ActiveSelector<T> currently
		// // stores a pointer.
		if (!p_newgamedialog->active_input.isCleared())
		{
			//TODO: HANDLE EVENTS FOR TEXT ENTRY INTO TEXTBOXES
			switch (p_eventhandler->type)
			{
				case sf::Event::MouseButtonPressed:
					if (p_eventhandler->mouseButton.button == sf::Mouse::Left ||
					        p_eventhandler->mouseButton.button == sf::Mouse::Right)
					{
						mousePos   = p_window->mapPixelToCoords(sf::Vector2i(p_eventhandler->mouseButton.x, p_eventhandler->mouseButton.y));
						// Clicked outside active input, clear it, and do primary event loop
						// // on the next iterary call of this function.
						if (p_newgamedialog->active_input.getActive()->insideControl(mousePos) == false)
							p_newgamedialog->active_input.clear();
					}
				break;
				case sf::Event::TextEntered:
					// If user presses enter, stop capturing events from with the textbox
					if (p_eventhandler->text.unicode == '\r')
					{
						p_newgamedialog->active_input.clear();
						return;
					}
					// Inserts characters into the textbox's buffer. Removes them on any captured backspace
					UIControls::input_modify_buffer((UIControls::TextInput*)p_newgamedialog->active_input.getActive(),
													p_eventhandler->text.unicode);
				break;
				default:
				break;
			}
			
			// WHILE ACTIVE_INPUT IS SET, BYPASS BELOW EVENT LOOP
			return;
		}
	
		switch (p_eventhandler->type)
		{
			case sf::Event::MouseMoved:
				control = NULL;
				if (p_newgamedialog->getControlFromMouse(mousePos, &control))
				{
					// 'UNHOVER' code *** For stuff like mouse leaving a control's region
					// See if there was a control hovered before
					// / lastcontrol will be null unless a control was hovered
					if (lastcontrol != NULL && lastcontrol != control)
					{
						try
						{
							lastcontrol->event_unhover((void*)lastcontrol, NULL);
						}
						catch (const std::bad_function_call &e)
						{
							std::cerr << e.what() << " for lastcontrol " << lastcontrol;
						}
						// last hovered control handled
						lastcontrol = NULL;
					}
					//Check if control even has a hover event
					if (control->mapped_events[0] == false)
						break;
					//It does, safely try to call it
					try
					{
						control->event_hover((void*)control, NULL);
						lastcontrol = control; // callback called, set last control hovered
					}
					catch (const std::bad_function_call &e)
					{
						std::cerr << e.what() << " for control " << control;
						lastcontrol = NULL; // callback failed, this control will never be the last one hovered
					}
				}
			break;
			case sf::Event::MouseButtonPressed:
				if (p_eventhandler->mouseButton.button == sf::Mouse::Right)
				{
					//we'll be returning to main menu so clear whatever the user entered
					p_newgamedialog->clearEnteredStuff();
					rightClickTransition(&p_menuwidget, p_newgamedialog);
				}
				if (p_eventhandler->mouseButton.button == sf::Mouse::Left)
				{
					control    = NULL;
					mousePos   = p_window->mapPixelToCoords(sf::Vector2i(p_eventhandler->mouseButton.x, p_eventhandler->mouseButton.y));
					if (p_newgamedialog->getControlFromMouse(mousePos, &control))
					{
						// Check if we have a click event first
						if (control->mapped_events[1] == false)
							break;
						
						// okay we do, try to call it
						try
						{
							control->event_clicked((void*)control, NULL);
						}
						catch (const std::bad_function_call &e)
						{
							std::cerr << e.what() << " for control " << control;
						}
					}
				}
			break;
			default:
			break;
		}
	}
}

void MainMenu::draw()
{
	p_window->draw(*p_background);
	p_window->draw(p_titletext);
	p_window->draw(p_menuwidget);
	p_window->draw(*p_loadmenuwidget);
	p_window->draw(*p_newgamedialog);
}

void MainMenu::load()
{
	sf::Vector2f vec;
	// main menu widget data
	float menusizex = p_window->getSize().x*0.59f;
	float menusizey = p_window->getSize().y*0.72f;
	// new game dialog data
	float dialogszx = (float) p_window->getSize().x/2;
	float dialogszy = p_window->getSize().y*0.61f;
	// title text data 
	p_titletext = sf::Text(p_maintitle, FontManager::getInstance()->getFontRef(FontManager::Title), 84);

	// save system setup
	p_savesys.populate();

	// background setup
	p_background = TiledBG::createAndLoad("res/mm_bgtile.png", p_window->getSize());

	// main menu widget setup
	p_menuwidget.setMenuColor(p_fillui);
	p_menuwidget.setMenuBorder(p_borderui, 5.0f);
	p_menuwidget.setAlignment(UIBase::AlignCenter | UIBase::AlignCenterVert);
	p_menuwidget.getAlignedPosition(vec, (float) p_window->getSize().x, (float) p_window->getSize().y, menusizex, menusizey, 0.0f, 0.0f);
	p_menuwidget.setDimensions(vec.x, vec.y, menusizex, menusizey);
	p_menuwidget.setMenuEntryPad(sf::Vector2f(-25.0f, 80.0f));
	p_menuwidget.addMenuEntry("new_game", p_newtitle, FontManager::UIReg, UIMenuEntry::AlignCenter, 52, p_textui, p_texthoverui);
	p_menuwidget.addMenuEntry("load_game", p_loadtitle, FontManager::UIReg, UIMenuEntry::AlignCenter, 52, p_textui, p_texthoverui);
	p_menuwidget.addMenuEntry("quit_game", p_quittitle, FontManager::UIReg, UIMenuEntry::AlignCenter, 52, p_textui, p_texthoverui);
	// main menu widget setup :: add event handlers
	using namespace std::placeholders;
	p_menuwidget["new_game"]->event_clicked  = std::bind(&MainMenu::clickedNewGame, this, _1);
	p_menuwidget["load_game"]->event_clicked = std::bind(&MainMenu::clickedLoadGame, this, _1);
	p_menuwidget["quit_game"]->event_clicked = std::bind(&MainMenu::clickedQuitGame, this, _1);


	// title text setup
	p_titletext.setColor(sf::Color::Green);
	p_titletext.setPosition(p_window->getSize().x*0.4f, 0.0f);

	// load menu setup
	p_loadmenuwidget = createLoadMenu(menusizex, menusizey, 0.0f, 20.0f);
	
	// new game dialog setup
	p_newgamedialog  = createNewGameDlg("STARTING NEW GAME...", dialogszx, dialogszy);

	// Set Active Menu
	p_activemenu.setActive(&p_menuwidget);

}

void MainMenu::unload()
{
	if (p_background != NULL)
		delete p_background;

	if (p_loadmenuwidget != NULL)
		delete p_loadmenuwidget;
		
	if (p_newgamedialog != NULL)
		delete p_newgamedialog;
}

sf::RenderWindow *MainMenu::getWindow() const
{
	return p_window;
}

void MainMenu::setWindow(sf::RenderWindow *window)
{
	p_window = window;
}

sf::Event *MainMenu::getEventHand() const
{
	return p_eventhandler;
}

void MainMenu::setEventHand(sf::Event *event)
{
	p_eventhandler = event;
}

void MainMenu::rightClickTransition(UIBase *to, UIBase *from)
{
	p_activemenu.setActive(to);
	from->setVisible(false);
	to->setVisible(true);
}

void MainMenu::clickedNewGame(void *param)
{
	p_activemenu.setActive(p_newgamedialog);
	p_menuwidget.setVisible(false);
	p_newgamedialog->setVisible(true);
}

void MainMenu::clickedLoadGame(void *param)
{
	p_activemenu.setActive(p_loadmenuwidget);
	p_menuwidget.setVisible(false);
	p_loadmenuwidget->setVisible(true);
}

void MainMenu::clickedQuitGame(void *param)
{
	return p_window->close();
}

void MainMenu::clickedLoadSave(void *param)
{
	const int whichsave = *((int*)param);
	std::cout << "CLICKED:" << p_savesys.getNthSave(whichsave).savename << '\n';

}

void MainMenu::clickedReturnFromLoad(void *param)
{
	p_activemenu.setActive(&p_menuwidget);
	p_loadmenuwidget->setVisible(false);
	p_menuwidget.setVisible(true);
}

UIMenu *MainMenu::createLoadMenu(float sizex, float sizey, float padx, float pady)
{
	sf::Vector2f vec;
	UIMenu *loadmenu = new UIMenu();
	const int total_saves = p_savesys.getNumSaves();

	// CREATED NON VISIBLE
	loadmenu->setVisible(false);
	// CREATED CENTER OF SCENE
	loadmenu->setAlignment(UIBase::AlignCenter | UIBase::AlignCenterVert);
	// COLOR IT
	loadmenu->setMenuColor(p_fillui);
	loadmenu->setMenuBorder(p_borderui, 5.0f);

	// POSITIONING CODE
	loadmenu->getAlignedPosition(vec, (float) p_window->getSize().x, (float) p_window->getSize().y, sizex, sizey, 0.0f, 0.0f);
	loadmenu->setDimensions(vec.x, vec.y, sizex, sizey);
	loadmenu->setMenuEntryPad(sf::Vector2f(padx, pady));

	// CREATE ENTRIES BASED ON CONTENTS OF SAVE DIRECTORY
	// IF EMPTY, A NON-INTERACTABLE, INFORMATIVE ENTRY WILL BE CREATED INSTEAD
	if (total_saves != 0)
	{
		for (int i = 0; i < total_saves; i++)
		{
			try
			{
				// Create menu entry for each save
				auto save = p_savesys.getNthSave(i);
				loadmenu->addMenuEntry(save.savepath, save.savename, FontManager::UIReg, UIMenuEntry::AlignCenter, 48, p_textui, p_texthoverui);
				// Register clicked event handler for each save's entry
				using namespace std::placeholders;
				(*loadmenu)[i]->event_clicked = std::bind(&MainMenu::clickedLoadSave, this, _1);
			}
			catch (const std::exception &e)
			{
			std::cerr << e.what() << " : " << i << '\n';
			}
		}
	}
	else
	{
		loadmenu->addMenuEntry("nsf", "No saves found...", FontManager::UIReg, UIMenuEntry::AlignCenter, 48, p_textui, p_textui);
	}

	// Return to main menu button will always be created last
	loadmenu->addMenuEntry("return_main", "Return", FontManager::UIHead, UIMenuEntry::AlignCenter, 52, p_textui, p_texthoverui);
	// Register its clicky
	using namespace std::placeholders;
	(*loadmenu)["return_main"]->event_clicked = std::bind(&MainMenu::clickedReturnFromLoad, this, _1);

	return loadmenu;
}

NewGameDialog *MainMenu::createNewGameDlg(const sf::String &dlgtitle, float sizex, float sizey)
{
	sf::Vector2f vec;
	sf::Vector2f vec2;
	float ctrl_x;
	NewGameDialog *dialog = new NewGameDialog();
	
	// SET DIALOG TITLE
	dialog->dialog_title = dlgtitle;
	// CENTER O' SCENE
	dialog->setAlignment(UIBase::AlignCenter | UIBase::AlignCenterVert);
	// COLOR IT
	dialog->setColors(p_texthoverui, p_borderui, p_fillui);
	// POSITION & SIZE IT
	dialog->getAlignedPosition(vec, (float) p_window->getSize().x, (float) p_window->getSize().y, sizex, sizey, 0.0f, 0.0f);
	dialog->setPosition(vec);
	dialog->setOrigin(vec);
	dialog->setSize(sizex, sizey);

	// CONTROL SETUP -- PROMPT1
	dialog->prompt1.setAlignment(UIBase::AlignLeft);
	dialog->prompt1.setStyle(p_textui, FontManager::getInstance()->getFontRef(FontManager::UIReg), 14);
	dialog->prompt1.setContents("Welcome to STSG. It's time for you to customize yourself & your band of mercenaries.");
	dialog->prompt1.setPadding(5.0f, 0.0f);
	vec2 = dialog->prompt1.getSize();
	dialog->prompt1.getAlignedPosition(vec,sizex,sizey,vec2.x,vec2.y,dialog->getOrigin().x,dialog->getOrigin().y+25.0f);
	dialog->prompt1.setPosition(vec);
	dialog->addControl<UIControls::Text>(dialog->prompt1);

	//CONTROL SETUP -- PLAYER_LABEL
	dialog->player_label.setAlignment(UIBase::AlignLeft);
	dialog->player_label.setStyle(p_textui, FontManager::getInstance()->getFontRef(FontManager::UIReg), 14);
	dialog->player_label.setContents("Your Name:");
	dialog->player_label.setPadding(5.0f, 0.0f);
	vec2 = dialog->player_label.getSize();
	ctrl_x = vec2.x; // for layout, we put the next textbox beside us
	dialog->player_label.getAlignedPosition(vec, sizex, sizey, vec2.x, vec2.y, dialog->getOrigin().x, dialog->getOrigin().y+50.0f);
	dialog->player_label.setPosition(vec);
	dialog->addControl<UIControls::Text>(dialog->player_label);
	
	// CONTROL SETUP -- PLAYER_NAME INPUT
	dialog->player_name.setAlignment(UIBase::AlignLeft);
	dialog->player_name.setColors(p_texthoverui, p_textui);
	dialog->player_name.setPadding(8.0f, 0.0f);
	vec2 = dialog->player_name.getSize();
	dialog->player_name.getAlignedPosition(vec, sizex, sizey, vec2.x, vec2.y, dialog->getOrigin().x+ctrl_x, dialog->getOrigin().y+50.0f);
	dialog->player_name.setPosition(vec);
	// / // EVENTS FOR PLAYER_NAME INPUT
	using namespace std::placeholders;
	dialog->player_name.mapped_events[0] = true; // has hover event
	dialog->player_name.event_hover = std::bind(&NewGameDialog::hoverTextBox, dialog, &dialog->player_name, _2);
	dialog->player_name.mapped_events[1] = true; // has clicked event
	dialog->player_name.event_clicked = std::bind(&NewGameDialog::clickedTextBox, dialog, &dialog->player_name, _2);
	dialog->player_name.mapped_events[2] = true; // has unhover event
	dialog->player_name.event_unhover = std::bind(&NewGameDialog::unhoverTextBox, dialog, &dialog->player_name, _2);
	dialog->addControl<UIControls::TextInput>(dialog->player_name);
	
	//CONTROL SETUP -- GROUP_LABEL
	dialog->group_label.setAlignment(UIBase::AlignLeft);
	dialog->group_label.setStyle(p_textui, FontManager::getInstance()->getFontRef(FontManager::UIReg), 14);
	dialog->group_label.setContents("Group Name:");
	dialog->group_label.setPadding(5.0f, 0.0f);
	vec2 = dialog->group_label.getSize();
	ctrl_x = vec2.x; // for layout, we put the next textbox beside us
	dialog->group_label.getAlignedPosition(vec, sizex, sizey, vec2.x, vec2.y, dialog->getOrigin().x, dialog->getOrigin().y+90.0f);
	dialog->group_label.setPosition(vec);
	dialog->addControl<UIControls::Text>(dialog->group_label);

	//CONTROL SETUP -- GROUP_NAME
	dialog->group_name.setAlignment(UIBase::AlignLeft);
	dialog->group_name.setColors(p_texthoverui, p_textui);
	dialog->group_name.setPadding(13.0f, 0.0f);
	vec2 = dialog->group_name.getSize();
	dialog->group_name.getAlignedPosition(vec, sizex, sizey, vec2.x, vec2.y, dialog->getOrigin().x+ctrl_x, dialog->getOrigin().y+90.0f);
	dialog->group_name.setPosition(vec);
	dialog->group_name.mapped_events[0] = true; // hover event yes
	dialog->group_name.event_hover = std::bind(&NewGameDialog::hoverTextBox, dialog, &dialog->group_name, _2);
	dialog->group_name.mapped_events[1] = true; // has clicked event
	dialog->group_name.event_clicked = std::bind(&NewGameDialog::clickedTextBox, dialog, &dialog->group_name, _2);
	dialog->group_name.mapped_events[2] = true; // unhover event yes
	dialog->group_name.event_unhover = std::bind(&NewGameDialog::unhoverTextBox, dialog, &dialog->group_name, _2);
	dialog->addControl<UIControls::TextInput>(dialog->group_name);

	return dialog;
}

NewGameDialog::NewGameDialog()
	: player_name(20), group_name(20)
{

}

NewGameDialog::~NewGameDialog()
{
}

void NewGameDialog::hoverTextBox(void *control, void *param)
{
	UIControls::TextInput *me = (UIControls::TextInput*) control;
	sf::Color color(SceneManager::getConfig()->color_uitexthover);
	
	me->setColors(UIControls::TextInput::BDColor, color);
}

void NewGameDialog::unhoverTextBox(void *control, void *param)
{
	UIControls::TextInput *me = (UIControls::TextInput*) control;
	sf::Color color(SceneManager::getConfig()->color_uitext);

	me->setColors(UIControls::TextInput::BDColor, color);
}

void NewGameDialog::clickedTextBox(void *control, void *param)
{
	UIControls::TextInput *me = (UIControls::TextInput*) control;
	
	// set text box as active input for the dialog
	// // making its events handled first and rather than its parent
	active_input.setActive(me);
}

void NewGameDialog::clearEnteredStuff()
{
	player_name.buffer.clear();
	group_name.buffer.clear();
}
