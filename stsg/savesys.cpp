#include <iostream>
#include "savesys.h"

SaveSystem::SaveSystem()
	: p_savpath("./saves/"), p_numsaves(0)
{

}

SaveSystem::~SaveSystem()
{

}

const int SaveSystem::getNumSaves() const
{
	return p_numsaves;
}

void SaveSystem::populate()
{
	bfs::directory_iterator dir_end;

	for (bfs::directory_iterator dir_itr(p_savpath); dir_itr != dir_end; ++dir_itr)
	{
		try
		{
			auto cur_item = dir_itr->path();
		
			if (bfs::is_directory(dir_itr->status()) ||
				bfs::is_regular_file(dir_itr->status()) == false)
				continue;
			/* Ctor Arg 1: Save Path, Arg 2: Save Name -- mostly for UI menu stuff, */
			/**TODO: GET SAVENAME FROM INSIDE THE SAVE FILE INSTEAD OF THE FILENAME **/
			p_saves.emplace_back(SaveFile(cur_item.string(), cur_item.filename().string()));
			p_numsaves++;
		}
		catch (const std::exception &e)
		{
			std::cerr << "in SaveSystem::populate() " << e.what() << " with " << dir_itr->path().filename() << '\n';
		}
	}

}

SaveFile &SaveSystem::getNthSave(const int index)
{
	if (index < 0 || index > getNumSaves())
		throw std::out_of_range("Out of Range Save Index");

	return p_saves.at(index);
}

/*
 * * SaveFile * *
 */

SaveFile::SaveFile()
{

}

SaveFile::SaveFile(const std::string &savpath)
{
	savepath = savpath;
}

SaveFile::SaveFile(const std::string &savpath, const std::string &savname)
{
	savepath = savpath;
	savename = savname;
}

SaveFile::~SaveFile()
{

}
