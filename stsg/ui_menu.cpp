#include <iostream>
#include "ui_menu.h"

UIMenu::UIMenu()
	: p_entriespadding(0.0f, 0.0f)
{
	p_offsety = 0.0f;
	p_largestdmx_x = 0;
}

UIMenu::~UIMenu()
{

}

void UIMenu::setMenuColor(const sf::Color &bgcolor)
{
	return p_mainsurface.setFillColor(bgcolor);
}

void UIMenu::setMenuBorder(const sf::Color &bordercolor, float pxthick)
{
	p_mainsurface.setOutlineColor(bordercolor);
	p_mainsurface.setOutlineThickness(pxthick);
}

void UIMenu::setDimensions(float posX, float posY, float sizeW, float sizeH)
{
	p_dimensions.left = posX;
	p_dimensions.top  = posY;
	p_dimensions.width = sizeW;
	p_dimensions.height = sizeH;
	p_mainsurface.setPosition(posX, posY);
	p_mainsurface.setSize(sf::Vector2f(sizeW, sizeH));
	this->setPosition(posX, posY);
	this->setOrigin(this->getPosition());
}

void UIMenu::setMenuEntryPad(const sf::Vector2f &padding)
{
	p_entriespadding = padding;
}

void UIMenu::addMenuEntry(const sf::String &refkey, const sf::String &entrycaption, EntryStyle font, AlignType alignment, int size, const sf::Color &norm, const sf::Color &hilite)
{
	sf::Vector2f origin = this->getOrigin();
	sf::Vector2f entrydmx = sf::Vector2f((float) entrycaption.getSize()*size, (float) size);
	sf::Vector2f entryfixed;
	UIMenuEntry  tempentry(refkey, entrycaption, font, alignment, size);

	// Adjust the to-be-added entry's dimensions to that of the largest added entry so
	// // that alignment can actually happen.
	if (p_largestdmx_x != 0)
	{
		if (p_largestdmx_x > entrydmx.x)
		{
			entrydmx.x = (float) p_largestdmx_x;
		}
		else if (entrydmx.x > p_largestdmx_x)
		{
			p_largestdmx_x = (int) entrydmx.x;
		}
	}
	else
		p_largestdmx_x = (int) entrydmx.x;

	tempentry.setPadding(p_entriespadding.x, p_entriespadding.y);
	tempentry.getAlignedPosition(entryfixed, p_dimensions.width, p_dimensions.height, entrydmx.x, entrydmx.y, origin.x, origin.y+p_offsety);
	p_offsety += entrydmx.y+p_entriespadding.y;
	tempentry.caption.setPosition(entryfixed);
	tempentry.caption.setColor(norm);
	tempentry.unhighlighted = norm;
	tempentry.highlighted = hilite;

	p_entries.push_back(tempentry);
}

int UIMenu::getEntryByMouse(const sf::Vector2f &mousePos)
{
	if (p_entries.size() == 0 || isVisible() == false)
		return -1;

	for (int i = 0; i < p_entries.size(); ++i)
	{
		sf::Vector2f localpoint = mousePos;
		localpoint += p_entries[i].getOrigin();
		localpoint -= p_entries[i].getPosition();
		//Fix for entries with X padding value set
		localpoint.x += fabs(1.1f*p_entries[i].getPadding(UI_XPADDING));

		if (localpoint.x < 0 ||
			localpoint.x > p_entries[i].getScale().x*p_entries[i].getEntryDimension(MENTRY_DIMENSIONX))
			continue;
		if (localpoint.y < 0 ||
			localpoint.y > p_entries[i].getScale().y*p_entries[i].getEntryDimension(MENTRY_DIMENSIONY))
			continue;

		return i;
	}

	return -1;
}

const int UIMenu::getNumEntries() const
{
	return p_entries.size();
}

void UIMenu::highlightEntry(const int entry)
{
	for (int i = 0; i < p_entries.size(); ++i)
	{
		if (i == entry)
		{
			p_entries[i].caption.setColor(p_entries[i].highlighted);
		}
		else
		{
			p_entries[i].caption.setColor(p_entries[i].unhighlighted);
		}
	}
}

void UIMenu::handleEntryClicked(const int entry, void *param)
{
	if (entry == -1)
		return;

	for (int i = 0; i < p_entries.size(); ++i)
	{
		if (i != entry)
			continue;

		try
		{
			p_entries[i].event_clicked(param);
		}
		catch (const std::bad_function_call &e)
		{
			std::cerr << e.what() << " for clicked: " << p_entries[i].referenceid.toAnsiString() << '\n';
		}
	}
}

void UIMenu::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (!isVisible())
		return;

	target.draw(p_mainsurface, states);

	for (auto entry = p_entries.begin(); entry != p_entries.end(); ++entry)
	{
		if (entry->isVisible() == false)
			continue;

		target.draw(entry->caption, states);
	}
}

UIMenuEntry *UIMenu::operator[](sf::String id) const
{
	UIMenuEntry *returnme = NULL;

	for (auto entry = p_entries.begin(); entry != p_entries.end(); ++entry)
	{
		if (entry->referenceid == id)
		{
			returnme = &(*entry);
			break;
		}

	}

	if (returnme == NULL)
		throw std::out_of_range(id);

	return returnme;
}

UIMenuEntry *UIMenu::operator[](const int index) const
{
	return const_cast<UIMenuEntry*>(&p_entries.at(index));
}

UIMenuEntry::UIMenuEntry(const sf::String &refid, const sf::String &entrystring, EntryStyle textstyle, AlignType textalign, int textsize)
	: UIBase(textalign)
{
	referenceid = refid;
	caption_string = entrystring;
	caption = sf::Text(entrystring, FontManager::getInstance()->getFontRef(textstyle), textsize);
	event_clicked = nullptr;

}

UIMenuEntry::~UIMenuEntry()
{

}

float UIMenuEntry::getEntryDimension(uint8_t xory)
{
	float result = 0.0f;

	if (xory == MENTRY_DIMENSIONX)
	{
		result = (float) caption.getCharacterSize()*caption.getString().getSize();
		return result;
	}
	if (xory == MENTRY_DIMENSIONY)
	{
		result = (float) caption.getCharacterSize();
		return result;
	}

	return result;
}

const sf::Vector2f &UIMenuEntry::getPosition() const
{
	return caption.getPosition();
}

const sf::Vector2f &UIMenuEntry::getOrigin() const
{
	return caption.getOrigin();
}

const sf::Vector2f &UIMenuEntry::getScale() const
{
	return caption.getScale();
}
