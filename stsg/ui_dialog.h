#ifndef _UI_DIALOG_H
#define _UI_DIALOG_H

#include <SFML/Graphics.hpp>
#include <SFML/System/String.hpp>
#include "fontmgr.h"
#include "ui_base.h"
#include "ui_controls.h"

/*
 * An array of this is init'd in m_controls.
 * Derived classes of UIDialog simply add controls they want drawn & handled
 * * via addControl<T>, where T is that control's type
 */
struct DialogControls
{
	DialogControls(sf::Drawable *drawptr, UIControls::Control *uiptr)
	{
		as_drawn = drawptr;
		as_ui    = uiptr;
	}

	sf::Drawable			 *as_drawn;
	UIControls::Control		 *as_ui;
};

class UIDialog : public sf::Transformable, public sf::Drawable, public UIBase
{
public:
	UIDialog();
	virtual ~UIDialog();

	//Overrides FROM sf::Transformable
	virtual void setOrigin(const sf::Vector2f &origin);
	virtual void setPosition(const sf::Vector2f &position);
	//Overrides FROM sf::Drawable
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	//Overrides FROM UIBase
	virtual void setVisible(bool visibility);


	// Set the Colors of the Dialog Box
	void setColors(const sf::Color &titletext, const sf::Color &titlebar, const sf::Color &dialog);

	// Set the size of the Dialog Box
	void setSize(float szX, float szY);
	
	// Add control to be drawn and handled
	// T should be a drawable control unless you have a fetish for undefined behavior
	template<class T>
	void addControl(const T& control)
	{
		m_controls.emplace_back(DialogControls((sf::Drawable*)&control, (UIControls::Control*)&control));
	}

	// If true is returned, a pointer to the control object under the mouse shall be stored at `control`
	bool getControlFromMouse(const sf::Vector2f &mouse, UIControls::Control **control /*OUT*/);
	
	//Public Members
	sf::String dialog_title;	// // BOTH THESE CAN BE CHANGED ANYTIME,
	int		   size_title;		// // // THEY ARE CHK'D/UPDATED EACH FRAME
	
protected:
	sf::RectangleShape m_dialog;
	sf::RectangleShape m_titlebar;
	
	sf::Text		  *m_title;
	
	sf::Vector2f	   m_dialogsize;

	std::vector<DialogControls> m_controls;
};

#endif
